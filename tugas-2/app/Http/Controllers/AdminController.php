<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function test()
    {
        return 'berhasil masuk ke halaman admin';
    }
    public function route1()
    {
        return 'berhasil masuk ke halaman super admin';
    }
    public function route2()
    {
        return 'berhasil masuk ke halaman admin';
    }
    public function route3()
    {
        return 'berhasil masuk ke halaman user';
    }
}
