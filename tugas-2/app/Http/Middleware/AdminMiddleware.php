<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->route()->named('route-1')) {
            if(Auth::user()->isSuperAdmin()) {
                return $next($request);
            }
            else 
            {
                abort(403);
            }
        }
        else if ($request->route()->named('route-2')) {
            if(Auth::user()->isSuperAdmin() || Auth::user()->isAdmin()) {
                return $next($request);
            }
            else
            {
                abort(403);
            }
        }
        else {
            return $next($request);
        }
    }
}
